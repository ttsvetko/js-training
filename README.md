# JavaScript Training

During the training, we (Ivo masterR and me) will keep the repo updated with the lastest presentation or materials that we have prepared for each class.

Each class will have its own folder that contains the meterials from the class and for homework. We will try to prepare detailed instructions for homeworks. To keep things simple and organized you have to create your own folder(name it with your name) where to upload your homework version:

Lesson 1

    \- Presentation
    \- Homework
        \- Template
        \- Tsvetan Tsvetkov
        \- .....
Lesson 2

    \- Presentation
    \- Homework
        \- Template
        \- Tsvetan Tsvetkov
    