(function() {
    /*
        Function body goes here:
        
        1. Query all required elements - input, add button, list containers.
        
        2. Create a new promise and retrieve the tasks.json content. 
            When promise is resolved parse content using JSON.parse() method.
            Handle exceptions, too - show alert if there is an error.
            
            
            For each task from the json do:
            - Using the html structure as shown below, create a div element using 
             document.createElement method. Add a class name 'task' to the newly 
             created element(hint: use classList method add()). If task is comleted - 
             add another class name 'completed'. Add attributes as show in the example.
             
            - Create a new input element and set its type to checkbox.
            - Add event listenter to the checkbox to mark the taks as completed - 
                if task is marked as completed - move it under completed list.
                If task is marked as incompleted - move it under the TODOs list.
            - Create new div element and set its content.
            - Append elements created in step 4 & 5 to be child of the element from step 3.
         
            - The element created in step 3 should be append to the element with id="todosList".
            NOTE: If task is completed, then it should be appended to the element with id="completedList".
            
        2. Attach event listener to the Add button to add a new task using the instructions above.
            Use input value for task content.
            Clear the input when task is added.
        
        Task html structure:
        <div class="task completed" layout>
            <input type="checkbox"/>
            <div>TASK</div>
        </div>
            
        NOTE: class 'completed' should be added for completed tasks only
        
    */
}());